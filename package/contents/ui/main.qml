/*
 *  SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtMultimedia 5.15
import org.kde.plasma.networkmanagement 0.2 as PlasmaNM
import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.plasma.plasmoid 2.0
import Qt.labs.platform 1.1 as Labs
import org.kde.notification 1.0

Item {
    id: root

    property string artist
    property string artisturl
    property string song
    property string songurl
    property string title: Plasmoid.title
    property bool isError: false
    property string tooltipimage
    property string albumimage
    property string imglocal
    property string album
    property string albumurl
    property int lastPlay: 0
    property int view: 0

    property string tmpFolder: Labs.StandardPaths
        .writableLocation(Labs.StandardPaths.TempLocation)
        .toString()
        .substring("file://".length)

    property bool isConnected: networkStatus.networkStatus === i18nd("plasma_applet_org.kde.plasma.networkmanagement", "Connected")

    Plasmoid.backgroundHints: PlasmaCore.Types.DefaultBackground
                              | PlasmaCore.Types.ConfigurableBackground

    StationsModel {
        id: stationsModel
    }

    Component.onCompleted: {
        reloadStationsModel()
    }

    Connections {
        target: Plasmoid.configuration
        function onServersChanged() {
            playMusic.stop()
            reloadStationsModel()
        }
    }

    Connections {
        target: Plasmoid.configuration
        function onPanelChanged() {
            playMusic.stop()
        }
    }

   /*  PlasmaCore.DataSource {
        id: executable

        engine: "executable"
        connectedSources: []

        onNewData: {
            const exitCode = data["exit code"]
            const exitStatus = data["exit status"]
            const stdout = data["stdout"]
            const stderr = data["stderr"]
            exited(sourceName, exitCode, exitStatus, stdout, stderr)
            disconnectSource(sourceName)
        }

        function exec(cmd) {
            if (cmd) {
                connectSource(cmd)
            }
        }

        signal exited(string cmd, int exitCode, int exitStatus, string stdout, string stderr)
    } */

    PlasmaCore.DataSource {
        id: imageget

        engine: "executable"
        connectedSources: []

        onNewData: {
            const exitCode = data["exit code"]
            const exitStatus = data["exit status"]
            const stdout = data["stdout"]
            const stderr = data["stderr"]
            exited(sourceName, exitCode, exitStatus, stdout, stderr)
            disconnectSource(sourceName)
        }

        function exec(cmd) {
            if (cmd) {
                connectSource(cmd)
            }
        }

        signal exited(string cmd, int exitCode, int exitStatus, string stdout, string stderr)
    }

    Connections {
        target: imageget
        function onExited() {
            root.imglocal = tmpFolder + "/arp_image"
            createNotification(root.imglocal, root.artist, root.song)
        }
    }

    MediaPlayer {
        id: playMusic

        property string title: isPlaying() && playMusic.metaData.title
            ? playMusic.metaData.title : ""

        onTitleChanged: {
            if (isPlaying()) {
                getInfo(playMusic.metaData.title)
            }
        }

        onError: {
            isError = true
            errorTimer.start()
        }

        onStopped: {
            playMusic.stop()
            playMusic.source = ""
            root.title = Plasmoid.title
            root.albumimage = ""
            root.imglocal = ""
            root.tooltipimage = ""
            root.album = ""
            root.albumurl = ""
            root.artist = ""
            root.artisturl = ""
            root.song = ""
            root.songurl = ""
        }
        volume: 0.75
    }

    Timer {
        id: errorTimer

        running: false
        repeat: false
        interval: 5000
        onTriggered: {
            isError = false
        }
    }

    Plasmoid.compactRepresentation: CompactRepresentation {}

    Plasmoid.fullRepresentation: FullRepresentation {
        id: dialogItem

        anchors.fill: parent
        focus: true
    }

    function isPlaying() {
        return playMusic.playbackState === MediaPlayer.PlayingState
    }

    function reloadStationsModel() {
        playMusic.stop()
        stationsModel.clear()
        try {
            const servers = JSON.parse(Plasmoid.configuration.servers)
            for (const server of servers) {
                if (server.active) {
                    stationsModel.append(server)
                }
            }
        } catch (e) {
            console.log(e)
        }
    }

    PlasmaNM.NetworkStatus {
        id: networkStatus

        onNetworkStatusChanged: {
            if (networkStatus.networkStatus === i18nd("plasma_applet_org.kde.plasma.networkmanagement", "Connected")) {
                root.isConnected = true
            } else {
                console.log("Connection lost")
                root.isConnected = false
            }
        }
    }

    function getInfo(data) {
        if (data !== undefined && data.indexOf('-') !== -1) {
            let strings // Magic to check non-standard artists/songs like "A-ha - Minor earth major sky"
            if (data.indexOf(' - ') !== -1) {
                strings = data.split(' - ')
            } else {
                strings = data.split('-')
            }
            const artist = strings[0].trim(), song = strings[1].trim()
            if (Plasmoid.configuration.getinfo) {
                const xmlhttp = new XMLHttpRequest()
                const api_key = "ada39a6834a3be4d641cc1aec7e64d48"
                const url = `http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=${api_key}&artist=${encodeURIComponent(artist)}&track=${encodeURIComponent(song)}&autocorrect=1&format=json`
                xmlhttp.onreadystatechange = () => {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                        setInfo(JSON.parse(xmlhttp.responseText))
                    }
                }
                xmlhttp.open("GET", url)
                xmlhttp.send()

                function setInfo(arr) {
                    root.artist = (arr.track && arr.track.artist) ? (arr.track.artist.name) : artist
                    root.song = arr.track ? (arr.track.name) : song
                    root.title = isPlaying() ? root.artist + " - " + root.song : Plasmoid.title
                    root.artisturl = (arr.track && arr.track.artist) ? (arr.track.artist.url) : ""
                    root.songurl = (arr.track && arr.track.url) ? (arr.track.url) : ""

                    root.albumimage = arr.track && arr.track.album
                            && arr.track.album.image[2]['#text'] ? arr.track.album.image[2]['#text'] : ""
                    root.tooltipimage = arr.track && arr.track.album
                            && arr.track.album.image[0]['#text'] ? arr.track.album.image[0]['#text'] : ""
                    root.album = arr.track && arr.track.album
                            && arr.track.album.title
                            && arr.track.album.title !== "" ? arr.track.album.title : ""
                    root.albumurl = arr.track && arr.track.album
                            && arr.track.album.url
                            && arr.track.album.url !== "" ? arr.track.album.url : ""
                    
                }
            } else {
                root.artist = artist
                root.song = song
                root.imglocal = "audio-x-generic"
                // "{"  fix for some stations
                root.title = isPlaying() && playMusic.metaData.title && !playMusic.metaData.title.startsWith("{")
                    ? playMusic.metaData.title : Plasmoid.title
                if (Plasmoid.configuration.notification) {
                    createNotification("audio-x-generic", root.artist, root.song)
                }
            }
        } else {
            // "{"  fix for some stations
            root.title = isPlaying() && playMusic.metaData.title && !playMusic.metaData.title.startsWith("{")
                ? playMusic.metaData.title : Plasmoid.title
            if (Plasmoid.configuration.notification) {
                createNotification("audio-x-generic", "", root.title)
            }
        }
    }

    function refreshServer(index) {
        if (isPlaying() && playMusic.source == stationsModel.get(index).hostname && lastPlay === index) {
            playMusic.stop()
        } else {
            playMusic.stop()
            playMusic.source = stationsModel.get(index).hostname
            playMusic.play()
        }
    }
    
    Component {
        id: notificationComponent
        Notification {
            componentName: "plasma_workspace"
            eventId: "notification"
            urgency: Notification.LowUrgency
            autoDelete: true
        }
    }
    function createNotification(image, title, subtitle){
        var notification = notificationComponent.createObject(parent);
            notification.title = title
            notification.text = subtitle
            notification.iconName = image
            notification.sendEvent()
    }
}
