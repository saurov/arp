/*
 *  SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15
import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.plasma.plasmoid 2.0

MouseArea {
    id: panelIconWidget

    Plasmoid.toolTipMainText: ""
    Plasmoid.toolTipSubText: "" //Remove default tooltip

    property int wheelDelta: 0

    Layout.preferredWidth: Plasmoid.configuration.panel ? loader.item.childrenRect.width : height
    acceptedButtons: Qt.LeftButton | Qt.MiddleButton
    hoverEnabled: true

    onClicked: {
        if (mouse.button === Qt.MiddleButton) {
            if (Plasmoid.configuration.lastplay) {
                refreshServer(lastPlay)
            }
        } else {
            Plasmoid.expanded = !Plasmoid.expanded
        }
    }

    onWheel: {
        volumeTimer.restart()
        const delta = wheel.angleDelta.y || wheel.angleDelta.x
        wheelDelta += delta
        while (wheelDelta >= 120) {
            wheelDelta -= 120
            if (playMusic.volume < 1) {
                playMusic.volume += 0.05
            }
        }
        while (wheelDelta <= -120) {
            wheelDelta += 120
            if (playMusic.volume > 0) {
                playMusic.volume -= 0.05
            }
        }
        tooltip.mainText = playMusic.volume === 0 ? i18n("Audio") : i18n("Volume")
        tooltip.subText = playMusic.volume === 0 ? i18n("Muted") : Math.round(playMusic.volume * 100) + "%"
        tooltip.image = ""
        tooltip.icon = setVolumeIcon(playMusic.volume)
    }

    onEntered: {
        tooltip.showToolTip()
    }

    Connections {
        target: root
        function onTooltipimageChanged() {
            if (panelIconWidget.containsMouse) {
                tooltip.showToolTip()
            }
        }
    }

    Loader {
        id: loader

        source: Plasmoid.configuration.panel ? "Heading.qml" : "Icon.qml"
        asynchronous: true
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
    }

    PlasmaCore.ToolTipArea {
        id: tooltip

        anchors.horizontalCenter: parent.horizontalCenter
        onAboutToShow: {
            setToolTip()
        }
    }

    Timer {
        id: volumeTimer
        running: false
        repeat: false
        interval: PlasmaCore.Units.humanMoment
        onTriggered: {
            setToolTip()
        }
    }

    function setToolTip() {
        const reallyPlaying = isPlaying() && playMusic.status === MediaPlayer.Buffered

        tooltip.mainText = root.artist !== "" ? root.artist : root.song === "" && reallyPlaying
                        && root.title !== Plasmoid.title ? root.title : Plasmoid.title
        tooltip.subText = root.song !== "" ? root.song : root.song === "" && reallyPlaying
                        && root.title !== Plasmoid.title ? "" : Plasmoid.metaData.description
        tooltip.icon = reallyPlaying ? "audio-x-generic" : "audio-radio"
        tooltip.image = root.artist !== "" ? root.tooltipimage : ""
    }

    function setVolumeIcon(volume) {
        let suffix
        if (volume <= 0.0) {
            suffix = "-muted"
        } else if (volume <= 0.25) {
            suffix = "-low"
        } else if (volume <= 0.75) {
            suffix = "-medium"
        } else {
            suffix = "-high"
        }
        return "audio-volume" + suffix
    }
}
