#!/bin/bash

# SPDX-FileCopyrightText: 2023 ivan tkachenko <me@ratijas.tk>
#
# SPDX-License-Identifier: LGPL-2.0-or-later

cd "$( dirname "${BASH_SOURCE[0]}" )"

# KDE scripts expect certain directory structure.
# This modification makes it compatible with merge_all.sh script.
export podir=tmp/templates
mkdir -p $podir

extract-messages.sh
